import { socket, username } from "./game.mjs";
import { createElement, addClass, removeClass } from "./helper.mjs";

export const startTimer = (time) => {
  document.getElementsByClassName('game-field')[0].innerHTML = '';
  document.getElementById('room-back-button').style.display = 'none';
  document.getElementById('room-title').style.marginBottom = '60px';

  let temp = time;

  let timerId = setInterval(() => {
    if (temp>=0) { 
      document.getElementsByClassName('game-field')[0].innerHTML = temp;
    }
    --temp; 
  }, 1000);

  setTimeout(() => { 
    clearInterval(timerId);
    socket.emit('START_GAME') 
  }, time*1000+1000);
}

export const showDocuments = (text, time) => {

  const gameTimer = createElement({
    tagName: "div",
    className: "game-field-timer"
  })
  document.getElementsByClassName('game-field')[0].innerHTML = ''
  document.getElementsByClassName('game-field')[0].appendChild(gameTimer);

  let temp = time;
  let isTime = true;

  let timerId = setInterval(() => {
    if (temp>=0) { 
      gameTimer.innerHTML = `${temp} seconds left`;
    }
    --temp; 
  }, 1000);

  setTimeout(() => { 
    isTime = false;
    clearInterval(timerId);
    let userProgress = document.getElementsByClassName('game_user-progress-real')[0].style.width;
    
    socket.emit('END_GAME', {username, userProgress});
  }, time*1000+1000);

  const gameDoc = createElement({
    tagName: "div",
    className: "game-field-document"
  })
  gameDoc.innerHTML = text;
  document.getElementsByClassName('game-field')[0].appendChild(gameDoc);

  let textArray = text.split('');
  let increase = 100/textArray.length;
  let charCounter = 0;
  let i = 0;
  
  document.addEventListener("keypress", function(event) {

    if(event.key===textArray[0] && isTime) {
      i+=increase;
      charCounter++;
      document.getElementById(`${username}-progress`).style.backgroundColor = 'green'
      document.getElementById(`${username}-progress`).style.width = `${i}%`
      textArray.shift();
      gameDoc.innerHTML = `<span>${text.substring(0, charCounter)}</span>${text.substring(charCounter,text.length)}`;
    }

    if(!textArray.length) {
      const userProgress = document.getElementsByClassName('game_user-progress-real')[0].style.width;
      socket.emit('END_GAME', {username, userProgress});
    }
  })
}

export const showWinners = (user) => {
  console.log(user.username, user.progress)
  alert(`Winners: ${user.username}`)
}