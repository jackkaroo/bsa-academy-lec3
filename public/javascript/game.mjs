import { createElement, addClass, removeClass } from "./helper.mjs";
import { createGameWindow, backToRooms, showUserIsReady, showUserIsNotReady} from "./room.mjs";
import { startTimer, showDocuments, showWinners } from "./start_game.mjs"

export const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

export const socket = io("http://localhost:3002/game", { query: { username } });

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

document.getElementById('rooms-title').innerHTML = `Hello, ${username.capitalize()}! Join room or create new!`

const roomsContainer = document.getElementById("rooms-container");
const roomsCreateButton = document.getElementById("rooms_create-button");
let activeRoomId = null;

const setActiveRoomId = roomId => {
  activeRoomId = roomId;
};

const onCreateRoom = () => {
  // if (activeRoomId === roomId) {
  //   return;
  // }
  const title = document.getElementById('rooms_create-title').value;
  if(!title) {
    document.getElementById('rooms_create-title-wrong').style.display = "flex"
    return;
  }
  else document.getElementById('rooms_create-title-wrong').style.display = "none";
  alert(`New room "${title}" added`);
  document.getElementById('rooms_create-title').value = ''
  socket.emit("CREATE_ROOM", title);
};

roomsCreateButton.addEventListener("click", onCreateRoom);

const createRoom = roomId => {
  const roomWrapper = createElement({
    tagName: "div",
    className: "room_item-wrapper",
    attributes: { id: roomId }
  });

  const roomTitle = createElement({
    tagName: "div",
    className: "room_item-title",
    attributes: { id: roomId }
  });
  roomTitle.innerText = roomId;
  roomWrapper.appendChild(roomTitle);

  const roomButton = createElement({
    tagName: "button",
    className: "room_item-button button",
    attributes: { id: roomId }
  });
  roomButton.innerHTML = 'Join';

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", roomId);
  };

  roomButton.addEventListener("click", onJoinRoom);
  roomWrapper.appendChild(roomButton);

  roomTitle.innerText = roomId;
  return roomWrapper;
};

const updateRooms = rooms => {
  const allRooms = rooms.map(createRoom);
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
};


const joinRoomDone = ({roomId}, usersArray) => {
 
  const newRoomElement = document.getElementById(roomId);
  addClass(newRoomElement, "active");

  if (activeRoomId) {
    const previousRoomElement = document.getElementById(activeRoomId);
    removeClass(previousRoomElement, "active");
  }

  setActiveRoomId(roomId);
  createGameWindow(roomId, usersArray);
};

socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("BACK_ROOMS_DONE", backToRooms);
socket.on("UPDATE_ROOM", createGameWindow);

socket.on("USER_NOT_READY_DONE", showUserIsNotReady);
socket.on("USER_READY_DONE", showUserIsReady);

socket.on("START_TIMER", startTimer)
socket.on("SHOW_DOC", showDocuments)
socket.on("END_GAME_RESULTS", showWinners)


