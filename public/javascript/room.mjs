import { socket, username } from "./game.mjs";
import { createElement, addClass, removeClass } from "./helper.mjs";

const onBackToRooms = () => {
  socket.emit("BACK_ROOMS");
};

export const backToRooms = () => {
  document.getElementById('rooms-page').style.display = 'block';
  document.getElementById('game-page').style.display = 'none';
  document.getElementById('game_users-container').innerHTML = ''
  document.getElementById('game-right').innerHTML = ''
}

const onReady = () => {
  const button = document.getElementsByClassName('game-start-button')[0];
  if(!document.getElementsByClassName('not-ready')[0]) {
    console.log("+")
    button.innerHTML = 'Not ready';
    addClass(button, 'not-ready');
    socket.emit("USER_READY", username )
  } else {
    console.log("-")
    button.innerHTML = 'Ready';
    removeClass(button,'not-ready')
    socket.emit("USER_NOT_READY", username )
  }
}

function updateGameWindowDom(roomId) { 
  document.getElementById('rooms-page').style.display = 'none';
  document.getElementById('game-page').style.display = 'block';

  document.getElementById('room-title').innerHTML = roomId;

  const backButton = document.getElementById('room-back-button');
  backButton.addEventListener('click', onBackToRooms);

  if(!document.getElementsByClassName('game-field')[0]) {
    createGameField()
  }
}

function createGameField() {
  const gameField = createElement({
    tagName: "div",
    className: "game-field"
  })
  document.getElementById('game-right').appendChild(gameField);

  const gameButton = createElement({
    tagName: "button",
    className: "button game-start-button"
  })
  gameButton.innerHTML = 'Ready';
  gameButton.addEventListener('click', onReady);
  gameField.appendChild(gameButton);
}

function createUsers(usersArray) {

  usersArray.map(user => {
    const userWrapper = createElement({
      tagName: "div",
      className: "game_user-wrapper"
    })
    document.getElementById('game_users-container').appendChild(userWrapper);

    const userTitle = createElement({
      tagName: "div",
      className: "game_user-title",
      attributes: { id: user.username }
    })
    if(user.username===username) {
      userTitle.innerHTML = `${user.username} (you)`;
    }
    else userTitle.innerHTML = user.username;
    userWrapper.appendChild(userTitle)

    const userProgress = createElement({
      tagName: "div",
      className: "game_user-progress"
    })
    userWrapper.appendChild(userProgress);

    const userProgressReal = createElement({
      tagName: "div",
      className: "game_user-progress-real",
      attributes: { id: `${user.username}-progress` }
    })
    userProgress.appendChild(userProgressReal);
  })
}

export const createGameWindow = (roomId, usersArray) => {
  document.getElementById('game_users-container').innerHTML = ''
  updateGameWindowDom(roomId);
  createUsers(usersArray);
}

export const showUserIsReady = user => {
  document.getElementById(`${user.username}`).style.color = 'green';
  
}

export const showUserIsNotReady = user => {
  document.getElementById(`${user.username}`).style.color = 'red'
}




